# Mampu menunjukkan dan menjelaskan penerapan setiap poin dari SOLID Design Principle

```
import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import { schema,rules } from '@ioc:Adonis/Core/Validator'
import User from 'App/Models/User'

export default class AuthController {
    public async register ({request,response}:HttpContextContract){
        try{
            const registerValidator = schema.create({
                email: schema.string({},[
                    rules.email(),
                    rules.unique(
                        {
                           table:"users",
                           column:"email"
                        })
                ]),
                password: schema.string({},[
                    rules.minLength(6)
                ]),
                username :schema.string(),
                no_telp: schema.string(),
            })
            const payload = await request.validate({schema:registerValidator})
            await User.create(payload)
            
            return response.created({
                massage: "Registrasi Berhasil"
            })
        }catch(error){
            return response.badRequest({
                massage:error.massage
            })
        } 
    }
    
    
    public async login({request, response,auth} :HttpContextContract){
        try{
            const loginValidation = schema.create({
                email: schema.string(),
                password: schema.string()
            })
            await request .validate({schema:loginValidation})
    
            const email = request.input('email')
            const password = request.input('password')
    
            const token = await auth.use('api').attempt(email, password, {
              expiresIn :'1 Days'
            })
    
            return response.ok({
                massage:"Login Berhasil",
                token
            })
        }catch (error){
            console.error("Error :",error)
            return response.unauthorized({
                massage: 'Gaggal '
            })
        }
    
    }
    public async logout({auth,response}: HttpContextContract){
        const user = auth.user
        if(user){
            await auth.use('api').revoke()
            return response.ok({
                massage: 'Logout Berhasil'
            })
        }
        return response.badRequest({
            massage: 'Logout Gagal'
        })
     }
}


```

Berdasarkan kode yang diberikan, terlihat bahwa ada beberapa prinsip dari SOLID yang diterapkan:

Single Responsibility Principle (SRP):

Setiap fungsi dalam kelas AuthController bertanggung jawab hanya terhadap satu tugas yang spesifik. Misalnya, fungsi register digunakan untuk melakukan registrasi pengguna, login untuk melakukan proses login, dan logout untuk melakukan proses logout.
Open-Closed Principle (OCP):

Tidak ada contoh yang jelas dari penerapan OCP dalam kode yang diberikan. OCP mengusulkan bahwa entitas perangkat lunak harus terbuka untuk ekstensi (extension) namun tertutup untuk modifikasi (modification). Namun, kode tersebut masih dapat mengalami perubahan jika fitur-fitur baru ditambahkan atau dikembangkan.
Liskov Substitution Principle (LSP):

Tidak ada contoh yang jelas dari penerapan LSP dalam kode yang diberikan. LSP menyatakan bahwa objek dari kelas turunan harus dapat digunakan sebagai pengganti objek kelas induk tanpa mempengaruhi kebenaran program.
Interface Segregation Principle (ISP):

Tidak ada contoh yang jelas dari penerapan ISP dalam kode yang diberikan. ISP mengusulkan bahwa klien tidak boleh dipaksa untuk bergantung pada antarmuka yang tidak mereka gunakan.
Dependency Inversion Principle (DIP):

Tidak ada contoh yang jelas dari penerapan DIP dalam kode yang diberikan. DIP menyatakan bahwa kelas harus bergantung pada abstraksi, bukan implementasi konkrit.
