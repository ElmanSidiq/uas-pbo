# Mampu menunjukkan dan menjelaskan Design Pattern yang dipilih
```
import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import { schema,rules } from '@ioc:Adonis/Core/Validator'
import User from 'App/Models/User'

export default class AuthController {
    public async register ({request,response}:HttpContextContract){
        try{
            const registerValidator = schema.create({
                email: schema.string({},[
                    rules.email(),
                    rules.unique(
                        {
                           table:"users",
                           column:"email"
                        })
                ]),
                password: schema.string({},[
                    rules.minLength(6)
                ]),
                username :schema.string(),
                no_telp: schema.string(),
            })
            const payload = await request.validate({schema:registerValidator})
            await User.create(payload)
            
            return response.created({
                massage: "Registrasi Berhasil"
            })
        }catch(error){
            return response.badRequest({
                massage:error.massage
            })
        } 
    }
    
    
    public async login({request, response,auth} :HttpContextContract){
        try{
            const loginValidation = schema.create({
                email: schema.string(),
                password: schema.string()
            })
            await request .validate({schema:loginValidation})
    
            const email = request.input('email')
            const password = request.input('password')
    
            const token = await auth.use('api').attempt(email, password, {
              expiresIn :'1 Days'
            })
    
            return response.ok({
                massage:"Login Berhasil",
                token
            })
        }catch (error){
            console.error("Error :",error)
            return response.unauthorized({
                massage: 'Gaggal '
            })
        }
    
    }
    public async logout({auth,response}: HttpContextContract){
        const user = auth.user
        if(user){
            await auth.use('api').revoke()
            return response.ok({
                massage: 'Logout Berhasil'
            })
        }
        return response.badRequest({
            massage: 'Logout Gagal'
        })
     }
}


```

Di sini terdapat pola desain kontroler menggunakan Adonis.js, yang mencakup fungsi-fungsi seperti register, login, dan logout. Pola desain umum ini mengikuti konvensi yang umum digunakan dalam kerangka kerja Adonis.js. Ini adalah contoh implementasi pola desain MVC (Model-View-Controller), di mana fungsi-fungsi logika aplikasi terletak dalam kontroler.

Beberapa prinsip desain yang terlihat dalam kode tersebut:

Penggunaan class untuk mewakili kontroler dengan metode-metode sebagai aksi-aksi yang dapat diakses melalui rute HTTP.
Deklarasi metode-metode kontroler menggunakan sintaks TypeScript.
Penggunaan Adonis.js untuk mengelola HTTP konteks dan respons.
Penggunaan Adonis.js Validator untuk memvalidasi data input.
Penggunaan Model Adonis.js untuk berinteraksi dengan basis data.
Penggunaan async/await untuk menangani operasi yang asinkronus, seperti validasi, penciptaan pengguna, dan otentikasi.
Penggunaan objek response untuk mengirim respons HTTP dengan kode status dan payload yang sesuai.
Penggunaan objek request untuk mengakses data yang dikirim oleh klien melalui permintaan HTTP.
Penggunaan objek auth untuk mengelola otentikasi dan otorisasi pengguna.
