# Mampu menunjukkan dan menjelaskan Graphical User Interface dari produk digital

![](GUI_Register_2023-07-15_230443.png)
![](GUI_Login_2023-07-15_230620.png)
![](GUI_Dasboard_2023-07-15_230906.png)

Register (Pendaftaran):
a. Pengguna membuka aplikasi atau situs web.
b. Mereka melihat tampilan awal yang mencakup opsi "Daftar" atau "Buat Akun Baru".
c. Ketika mereka memilih opsi tersebut, tampilan GUI pendaftaran muncul.
d. Di GUI pendaftaran, pengguna diminta untuk memasukkan informasi pribadi seperti  alamat email, password,nama dan no.Telp.
e. Setelah memasukkan informasi yang diperlukan, pengguna menekan tombol "Daftar" untuk melanjutkan.
f. Sistem memvalidasi informasi yang dimasukkan dan memeriksa apakah alamat email yang dimasukkan sudah terdaftar sebelumnya.
g. Jika semua informasi valid, pengguna berhasil mendaftar dan diarahkan ke halaman login.

Login (Masuk):
a. Pengguna membuka aplikasi atau situs web.
b. Mereka melihat tampilan awal yang mencakup opsi "Masuk" atau "Login".
c. Ketika mereka memilih opsi tersebut, tampilan GUI login muncul.
d. Di GUI login, pengguna diminta untuk memasukkan alamat email dan password yang sudah terdaftar sebelumnya.
e. Setelah memasukkan informasi yang diperlukan, pengguna menekan tombol "Masuk" atau "Login" untuk melanjutkan.
f. Sistem memeriksa kebenaran kombinasi alamat email dan kata sandi yang dimasukkan.
g. Jika informasi yang dimasukkan benar, pengguna berhasil masuk dan diarahkan ke halaman dashboard.

Dashboard:
a. Setelah pengguna berhasil login, mereka diarahkan ke halaman dashboard.

