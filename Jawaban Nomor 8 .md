# Mampu menunjukkan dan menjelaskan HTTP connection melalui GUI produk digital

```
async function register() {
    document.getElementById("register-form").addEventListener("submit", async function (event) {
        event.preventDefault();

        try {
            const email = document.getElementById("email").value;
            const password = document.getElementById("password").value;
            const nama = document.getElementById("username").value;
            const noTelp = document.getElementById("no-telp").value;


            if (!email || !password || !nama || !noTelp) {
                throw new Error("Mohon lengkapi semua field");
            }

            const data = {
                email: email,
                password: password,
                username: nama,
                no_telp: noTelp

            };

            const response = await fetch("http://127.0.0.1:3333/api/v1/register", {
                method: "POST",
                headers: {
                    "Content-Type": "application/json"
                },
                body: JSON.stringify(data)
            });

            if (response.ok) {
                const result = await response.json();
                window.location.href = "../login.html";
            } else {
                throw new Error("Terjadi kesalahan saat melakukan register");
            }
        } catch (error) {
            console.error("Error:", error);
            alert(error.message);
        }
    });
}

function login(event) {
    event.preventDefault();

    const email = document.getElementsByName("Email")[0].value;
    const password = document.getElementsByName("Password")[0].value;

    const requestBody = {
        email: email,
        password: password,
    };

    fetch("http://localhost:3333/api/v1/login", {
        method: "POST",
        headers: {
            "Content-Type": "application/json",
        },
        body: JSON.stringify(requestBody),
    })
        .then((response) => response.json())
        .then((data) => {
            const token = data.token.token;
            localStorage.setItem("token", token)
            // Simpan token ke localStorage atau tempat lain yang sesuai
            // Lakukan tindakan selanjutnya setelah login berhasil, seperti mengarahkan pengguna ke halaman dashboard
            // console.log("Login berhasil. Token:", token);
            window.location.href = "../dashboard.html";
        })
        .catch((error) => {
            console.error("Error:", error);
        });
}

function showDashboard() {
    document.querySelector(".login").style.display = "none";
    const dashboard = document.querySelector(".dashboard");
    dashboard.style.display = "block";
    document.getElementById("username").textContent = loggedInUser;
}

function logout() {
    const token = localStorage.getItem("token");
    fetch("http://localhost:3333/api/v1/logout", {
        method: "POST",
        headers: {
            "Content-Type": "application/json",
            "Authorization": `Bearer ${token}`
        },
    })
        .then((response) => response.json())
        .then((data) => {
            console.log("Logout berhasil");
            localStorage.removeItem("token")
            window.location.href = "../login.html";

            // Hapus token dari localStorage atau tempat penyimpanan lainnya
            // Lakukan tindakan selanjutnya setelah logout berhasil, seperti mengarahkan pengguna ke halaman login
        })
        .catch((error) => {
            console.error("Error:", error);
        });
}

document.addEventListener("DOMContentLoaded", () => {
    const form = document.getElementById("login-form");
    form.addEventListener("submit", login);

    const dashboard = document.querySelector(".dashboard");
    const token = localStorage.getItem("token");
    if (token) {
        dashboard.style.display = "block";
        const username = localStorage.getItem("username");
        document.getElementById("username").textContent = username;
    } else {
        dashboard.style.display = "none";
    }
});



```
Pada kode yang diberikan, terdapat beberapa contoh penggunaan HTTP untuk berkomunikasi antara aplikasi web frontend dengan backend menggunakan metode `fetch()` dalam JavaScript.

1. Pada fungsi `register()`, terdapat penggunaan HTTP POST untuk mendaftarkan pengguna baru ke backend. Fungsi ini mengambil data dari form pendaftaran, seperti email, password, nama, dan nomor telepon, kemudian mengirimnya ke URL "http://127.0.0.1:3333/api/v1/register" menggunakan metode POST dengan menggunakan `fetch()`. Data dikirim dalam format JSON dengan mengatur header "Content-Type" sebagai "application/json". Jika responsenya berhasil (response.ok), maka pengguna akan diarahkan ke halaman login.

2. Pada fungsi `login(event)`, terdapat penggunaan HTTP POST untuk proses login. Fungsi ini mengambil email dan password dari form login, kemudian mengirimnya ke URL "http://localhost:3333/api/v1/login" menggunakan metode POST dengan menggunakan `fetch()`. Data dikirim dalam format JSON dan juga mengatur header "Content-Type" sebagai "application/json". Setelah mendapatkan responsenya, token yang diterima akan disimpan di localStorage dan pengguna akan diarahkan ke halaman dashboard.

3. Pada fungsi `logout()`, terdapat penggunaan HTTP POST untuk proses logout. Fungsi ini mengambil token yang disimpan di localStorage, kemudian mengirimnya ke URL "http://localhost:3333/api/v1/logout" menggunakan metode POST dengan menggunakan `fetch()`. Token dikirim sebagai bagian dari header "Authorization" dengan format "Bearer {token}". Jika responsenya berhasil, token akan dihapus dari localStorage dan pengguna akan diarahkan ke halaman login.

Selain itu, terdapat penggunaan `fetch()` pada saat memuat halaman menggunakan event "DOMContentLoaded". Fungsi ini akan memeriksa apakah token tersimpan di localStorage. Jika token ditemukan, maka halaman dashboard akan ditampilkan, dan jika tidak ditemukan, maka halaman login akan ditampilkan.
