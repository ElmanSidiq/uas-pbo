# Mampu menunjukkan dan menjelaskan konektivitas ke database
```
PORT=3333
HOST=0.0.0.0
NODE_ENV=development
APP_KEY=k4MnO0MHbP2MV0rKsBlZSdSTEQm8DztO
DRIVE_DISK=local
DB_CONNECTION=mysql
MYSQL_HOST=localhost
MYSQL_PORT=3306
MYSQL_USER=root
MYSQL_PASSWORD=
MYSQL_DB_NAME=uas_pbo
HASH_DRIVER=bcrypt

 ```



- DB_CONNECTION: Menyatakan jenis koneksi database yang digunakan, yaitu MySQL.
- MYSQL_HOST: Menyatakan host database yang digunakan, yaitu "localhost". Hal ini menandakan bahwa database MySQL berjalan di mesin yang sama dengan aplikasi yang sedang berjalan.
- MYSQL_PORT: Menyatakan port yang digunakan untuk koneksi database MySQL, yaitu 3306. Ini adalah port standar untuk MySQL.
- MYSQL_USER: Menyatakan pengguna (user) yang digunakan untuk mengakses database, yaitu "root". Dalam hal ini, pengguna yang memiliki akses penuh (superuser) digunakan.
- MYSQL_PASSWORD: Menyatakan kata sandi (password) yang digunakan untuk mengakses database. Dalam kode yang diberikan, tidak ada nilai yang ditetapkan untuk variabel ini, sehingga password tidak disediakan. Jika Anda ingin menggunakan password, Anda harus menentukan nilainya di sini.
- MYSQL_DB_NAME: Menyatakan nama database yang akan digunakan, yaitu "uas_pbo". Ini menunjukkan bahwa aplikasi akan terhubung ke database dengan nama tersebut.

Dengan menggunakan nilai-nilai tersebut, Kita dapat menggunakan library atau modul koneksi database di aplikasi Kita (misalnya, menggunakan MySQL Connector untuk Node.js) dan menggunakan informasi yang diberikan untuk terhubung ke database MySQL dengan menggunakan host "localhost", port 3306, pengguna "root" (jika Kita menggunakan password, Kita perlu menyediakan password yang benar) dan mengakses database "uas_pbo".
