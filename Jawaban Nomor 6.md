# Mampu menunjukkan dan menjelaskan pembuatan web service dan setiap operasi CRUD nya

```
import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import { schema,rules } from '@ioc:Adonis/Core/Validator'
import User from 'App/Models/User'

export default class AuthController {
    public async register ({request,response}:HttpContextContract){
        try{
            const registerValidator = schema.create({
                email: schema.string({},[
                    rules.email(),
                    rules.unique(
                        {
                           table:"users",
                           column:"email"
                        })
                ]),
                password: schema.string({},[
                    rules.minLength(6)
                ]),
                username :schema.string(),
                no_telp: schema.string(),
            })
            const payload = await request.validate({schema:registerValidator})
            await User.create(payload)
            
            return response.created({
                massage: "Registrasi Berhasil"
            })
        }catch(error){
            return response.badRequest({
                massage:error.massage
            })
        } 
    }
    

```

Code yang diberikan merupakan contoh implementasi dari sebuah web service menggunakan framework AdonisJS, yang bertujuan untuk melakukan operasi registrasi pengguna (register user).

```
import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import { schema,rules } from '@ioc:Adonis/Core/Validator'
import User from 'App/Models/User'

```
Pada bagian ini, kita mengimpor modul yang diperlukan untuk membuat web service. HttpContextContract adalah kontrak untuk objek konteks HTTP dalam AdonisJS, schema dan rules digunakan untuk validasi data input menggunakan Adonis Validator, dan User adalah model pengguna yang digunakan dalam aplikasi.

```
export default class AuthController {
    public async register ({request,response}:HttpContextContract){


```

Kode ini mendefinisikan kelas AuthController yang akan menangani operasi registrasi. Fungsi register di dalamnya adalah metode yang akan dijalankan ketika endpoint registrasi diakses.

```
try{
    const registerValidator = schema.create({
        email: schema.string({},[
            rules.email(),
            rules.unique(
                {
                    table:"users",
                    column:"email"
                })
        ]),
        password: schema.string({},[
            rules.minLength(6)
        ]),
        username :schema.string(),
        no_telp: schema.string(),
    })


```
Di dalam blok try, kita mendefinisikan skema validasi menggunakan schema.create dari Adonis Validator. Di sini, kita mendefinisikan bahwa data yang harus divalidasi adalah email, password, username, dan no_telp. Setiap field memiliki aturan validasi tertentu. Misalnya, email harus berupa email yang valid dan unik di dalam tabel users. password harus memiliki panjang minimal 6 karakter.

```
const payload = await request.validate({schema:registerValidator})


```
Setelah skema validasi dibuat, kita menggunakan request.validate untuk memvalidasi data input yang dikirimkan ke endpoint registrasi. Jika validasi berhasil, data input akan disimpan dalam variabel payload.

```
await User.create(payload)


```
Selanjutnya, kita menggunakan model User untuk membuat data pengguna baru dalam database. Data yang disimpan diambil dari payload yang telah divalidasi.

```
return response.created({
    massage: "Registrasi Berhasil"
})


```

Jika operasi registrasi berhasil, kita mengembalikan respon dengan kode status 201 (created) dan pesan "Registrasi Berhasil".

```
}catch(error){
    return response.badRequest({
        massage:error.massage
    })
} 


```


Jika terjadi kesalahan saat validasi atau operasi registrasi, kita menangkap kesalahan tersebut di blok catch dan mengembalikan respon dengan kode status 400 (bad request) dan pesan kesalahan yang diterima.
